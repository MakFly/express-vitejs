import express from 'express';
import morgan, { token } from 'morgan';
import dotenv from 'dotenv';
import { connected } from 'process';

const app = express();
dotenv.config();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"))

app.use("/api", (req, res, next) => {
  console.log("API Middleware called")
  res.status(200).send("API Middleware called")
})

app.use((error, req, res, next) => {
  console.log("Error Handling Middleware called")
  console.log('Path: ', req.path)
  console.error('Error: ', error)

  res.status(error.status || 500).send(error.message)
})

export const handler = app;